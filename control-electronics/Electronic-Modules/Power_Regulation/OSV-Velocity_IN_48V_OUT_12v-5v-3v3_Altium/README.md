# Power Module - OSV Velocity team

## Designer

Federico De Benedetti (OSV Velocity Team - INFN Milan Division)

## Main specifications

- Input voltage 48V 4.5A

- Output voltage:
    - 12v   50mA
    - 5v    500mA
    - 3v3   500mA
    
- Nominal overcurrent Fuse protection:
    - 6.3 A (FAST)